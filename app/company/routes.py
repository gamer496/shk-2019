from flask import render_template, redirect, url_for
from app.company import company
from flask_login import current_user, login_required
from app.company.forms import CompanyEditForm
from app import db
from flask_babel import _

@company.route('/company-edit-profile', methods = ["GET", "POST"])
@login_required
def company_edit_profile():
    form = CompanyEditForm()
    if form.validate_on_submit():
        current_user.company_link = form.company_link.data
        db.session.add(current_user)
        db.session.commit()
        return redirect(url_for('company_edit_profile'))
    form.company_link.data = current_user.company_link
    return render_template('edit_profile.html', title = _('Edit Profile'), form = form)