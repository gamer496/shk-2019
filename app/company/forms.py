from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from flask_babel import lazy_gettext as _l

class CompanyEditForm(FlaskForm):
    company_link = StringField(_l('Company Link'))
    submit = SubmitField(_l('Update'))
