from app import db

class Tag(db.Model):
    id              =db.Column      (db.Integer, primary_key = True)
    name            =db.Column      (db.String(100), unique = True)
    description     =db.Column      (db.String(250))

    def __repr__(self):
        return self.name