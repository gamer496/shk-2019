from flask import render_template, flash, redirect, url_for
from flask_login import login_user, logout_user, current_user
from flask_babel import _
from app.auth import auth
from app.auth.forms import LoginForm, RegistrationForm
from app.auth.models import Student, Company
from app.auth.email import send_auth_email
from app import db


@auth.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        userStudent = Student.query.filter_by(email_id=form.email_id.data).first()
        if userStudent is not None:
            send_auth_email(userStudent)
            flash(_("Verification email sent"))
            return redirect(url_for('auth.auth_mail_sent'))
        userCompany = Company.query.filter_by(email_id=form.email_id.data).first()
        if userCompany is not None:
            send_auth_email(userCompany)
            flash(_("Verification email sent"))
            return redirect(url_for('auth.auth_mail_sent'))
        flash(_('No such email.'))
        return redirect(url_for('login'))
    return render_template('login.html', title=_('Sign In'), form=form)


@auth.route('/mail', methods = ['GET'])
def auth_mail_sent():
    return render_template('mail_sent.html', title = _("Mail Sent"))


@auth.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('login'))


@auth.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        if form.is_company.data:
            user = Company()
        else:
            user = Student()
        user.email_id = form.email_id.data
        user.contact_number = form.contact_number.data
        user.description = form.description.data
        user.username = form.username.data
        db.session.add(user)
        db.session.commit()
        send_auth_email(user)
        flash(_('Congratulations, details submitted successfully. Auth token sent to email id'))
        return redirect(url_for('auth.auth_mail_sent'))
    return render_template('register.html', title=_('Register'), form=form)


@auth.route('/validate_auth/<token>', methods=['GET', 'POST'])
def validate_auth(token):
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    user_student = Student.verify_auth_token(token)
    if user_student:
        login_user(user_student)
        return redirect(url_for('index'))
    user_company = Company.verify_auth_token(token)
    if user_company:
        login_user(user_company)
        return redirect(url_for('index'))
    flash(_("Token not valid"))
    return redirect(url_for('login'))