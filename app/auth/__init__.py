from flask import Blueprint
from flask_admin.contrib.sqla import ModelView
from app import admin, db

auth = Blueprint("auth", __name__)

import app.auth.models, app.auth.routes

from app.auth.models import Student
from app.tags.models import Tag


class StudentModelView(ModelView):
    column_searchable_list = (Student.email_id, Tag.name)
    column_filters = (Student.email_id, Tag.name)


admin.add_view(StudentModelView(Student, db.session))
