from flask_wtf import FlaskForm
from wtforms import StringField, BooleanField, SubmitField, \
    TextAreaField
from wtforms.validators import ValidationError, DataRequired, Email
from flask_babel import _, lazy_gettext as _l
from app.auth.models import Company, Student


class LoginForm(FlaskForm):
    email_id = StringField(_l('Email Id'), validators=[DataRequired()])
    submit = SubmitField(_l('Sign In'))


class RegistrationForm(FlaskForm):
    email_id = StringField(_l('Email Id'), validators=[DataRequired(), Email()])
    contact_number = StringField(_l('Contact Number'), validators=[DataRequired()])
    description = TextAreaField(_l('Description'))
    username = StringField(_l('Name'))
    is_company = BooleanField("Register As Company?")
    submit = SubmitField(_l('Register'))

    def validate_email_id(self, email_id):
        userCompany = Company.query.filter_by(email_id =email_id.data).first()
        userStudent = Student.query.filter_by(email_id =email_id.data).first()
        if userCompany is not None or userStudent is not None:
            raise ValidationError(_('Please use a different email address.'))
    
    def validate_contact_number(self, contact_number):
        userCompany = Company.query.filter_by(contact_number=contact_number.data).first()
        userStudent = Student.query.filter_by(contact_number=contact_number.data).first()
        if userCompany is not None or userStudent is not None:
            raise ValidationError(_("Please use a different contact number"))
