from threading import Thread
from flask import render_template
from flask_mail import Message
from flask_babel import _
from app import mail
from flask import current_app as app


def send_async_email(app, msg):
    with app.app_context():
        mail.send(msg)


def send_email(subject, sender, recipients, text_body, html_body):
    msg = Message(subject, sender=sender, recipients=recipients)
    msg.body = text_body
    msg.html = html_body
    Thread(target=send_async_email, args=(app, msg)).start()
    mail.send(msg)


def send_auth_email(user):
    token = user.get_auth_token()
    send_email(_('[Skilled Grads] Log In'),
               sender=app.config['ADMINS'][0],
               recipients=[user.email_id],
               text_body=render_template('email/auth_verification.txt',
                                         user=user, token=token),
               html_body=render_template('email/auth_verification.html',
                                         user=user, token=token))
