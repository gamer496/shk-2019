from time import time

import jwt
from flask import current_app as app
from flask_login import UserMixin
from werkzeug.security import generate_password_hash,check_password_hash

from app import db,login


class Student(db.Model, UserMixin):
    id              =db.Column  (db.Integer, primary_key = True)
    email_id        =db.Column  (db.String(250), index = True, unique = True)
    username        =db.Column  (db.String(250))
    contact_number  =db.Column  (db.String(20), unique = True, index = True)
    description     =db.Column  (db.Text)
    git_link        =db.Column          (db.Text)
    google_link     =db.Column          (db.Text)
    tags            =db.relationship    ("Tag", secondary = "students_tags", backref = db.backref("students", lazy = "dynamic"))

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_auth_token(self, expires_in=600):
        return jwt.encode(
            {'verify_auth': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['verify_auth']
        except:
            return
        return Student.query.get(id)



class Company(db.Model, UserMixin):
    id              =db.Column  (db.Integer, primary_key = True)
    email_id        =db.Column  (db.String(250), index = True, unique = True)
    username        =db.Column  (db.String(250))
    contact_number  =db.Column  (db.String(20), unique = True, index = True)
    description     =db.Column  (db.Text)
    company_link    =db.Column  (db.Text)

    def __repr__(self):
        return '<User {}>'.format(self.username)

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def get_auth_token(self, expires_in=600):
        return jwt.encode(
            {'verify_auth': self.id, 'exp': time() + expires_in},
            app.config['SECRET_KEY'],
            algorithm='HS256').decode('utf-8')

    @staticmethod
    def verify_auth_token(token):
        try:
            id = jwt.decode(token, app.config['SECRET_KEY'],
                            algorithms=['HS256'])['verify_auth']
        except:
            return
        return Company.query.get(id)


@login.user_loader
def load_user(id):
    student = Student.query.get(int(id))
    if student is not None:
        return student
    return Company.query.get(int(id))


students_tags = db.Table("students_tags",
                    db.Column("student_id", db.Integer, db.ForeignKey("student.id")),
                    db.Column("tag_id", db.Integer, db.ForeignKey("tag.id"))
                    )