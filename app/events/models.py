from app import db

class Event(db.Model):
    id                  =db.Column(db.Integer, primary_key = True, unique = True)
    name                =db.Column(db.String(100))
    location            =db.Column(db.Text())
    google_location_link=db.Column(db.String(250))
    date                =db.Column(db.Date())
    time                =db.Column(db.Time())
    company_id          =db.Column(db.Integer, db.ForeignKey("company.id"))
    tags                =db.relationship('Tag', secondary = "events_tags", backref = db.backref("tags", lazy="dynamic"))
    students            =db.relationship('Student', secondary = "students_events", backref = db.backref("events", lazy = "dynamic"))

    def __repr__(self):
        return self.name

events_tags = db.Table("events_tags",
                db.Column("event_id", db.Integer, db.ForeignKey("event.id")),
                db.Column("tag_id", db.Integer, db.ForeignKey("tag.id"))
                )

students_events = db.Table("students_events",
                db.Column("student_id", db.Integer, db.ForeignKey("student.id")),
                db.Column("event_id", db.Integer, db.ForeignKey("event.id"))
                )
