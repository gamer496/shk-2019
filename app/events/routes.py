from flask import render_template, redirect, url_for, request, jsonify
from flask_login import current_user, login_required
from app import db
from app.events import events
from app.events.forms import EventForm
from flask_babel import _
from app.events.models import Event
from app.auth.models import Student
from app.tags.models import Tag


@events.route("/create_event", methods = ["GET", "POST"])
@login_required
def create_event():
    form = EventForm()
    form.tag_list.choices = [(str(tag.id), tag.name) for tag in Tag.query.all()]
    if form.validate_on_submit():
        event = Event()
        event.location = form.location.data
        event.google_location_link = form.google_map_link.data
        event.date = form.date.data
        event.time = form.time.data
        event.name = form.name.data
        tags = form.tag_list.data
        for tag in tags:
            t = Tag.query.get(int(tag))
            event.tags.append(t)
        event.company_id = current_user.id
        db.session.add(event)
        db.session.commit()
        return redirect(url_for('events.event_students_book', event_id = event.id))
    return render_template("create_event.html", title = _("Create Event"), form = form)


@events.route("/event_students_book/<event_id>", methods = ["GET"])
def event_students_book(event_id):
    event = Event.query.get(int(event_id))
    tags = event.tags
    student_set = set()
    for tag in tags:
        for student in tag.students:
            student_set.add(student)
    return render_template("event_students_book.html", title = _("Book Students"), students = list(student_set), event = event)

@events.route("/book_student", methods = ["POST"])
def book_student():
    data = request.get_json()
    event_id = data["event_id"]
    student_id = data["student_id"]
    event = Event.query.get(event_id)
    student = Student.query.get(student_id)
    event.students.append(student)
    db.session.add(event)
    db.session.commit()
    return jsonify({"msg": "success"})
