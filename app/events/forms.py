from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField, SelectMultipleField
from wtforms.fields.html5 import DateField, TimeField
from flask_babel import _, lazy_gettext as _l
from wtforms.validators import DataRequired

class EventForm(FlaskForm):
    name            = StringField(_l('Name'), validators = [DataRequired()])
    location        = TextAreaField(_l('Location'), validators=[DataRequired()])
    google_map_link = StringField(_l('Google Location Link'))
    date            = DateField(_l('Date of Event'), validators=[DataRequired()])
    time            = TimeField(_l("Time of Event"), validators=[DataRequired()])
    tag_list        = SelectMultipleField("Skills")
    submit = SubmitField(_l('Register Event'))
