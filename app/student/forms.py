from flask_wtf import FlaskForm
from wtforms import StringField, SelectMultipleField, SubmitField
from flask_babel import _, lazy_gettext as _l


class ProfileEditForm(FlaskForm):
    git_link = StringField(_l('Git Link'))
    google_link = StringField(_l('Google Link'))
    tags = SelectMultipleField("programming languages")
    submit = SubmitField(_l('Save'))
