from flask import render_template, redirect, url_for, flash
from flask_login import login_required, current_user
from flask_babel import _
from app.student import student
from app.student.forms import ProfileEditForm
from app import db
from app.tags.models import Tag


@student.route('/edit-student-profile', methods = ['GET', 'POST'])
@login_required
def edit_student_profile():
    form = ProfileEditForm()
    form.tags.choices = [(str(tag.id), tag.name) for tag in Tag.query.all()]
    if form.validate_on_submit():
        student = current_user
        student.git_link = form.git_link.data
        student.google_link = form.git_link.data
        all_selected_tags = form.tags.data
        for tag in all_selected_tags:
            t = Tag.query.get(int(tag))
            student.tags.append(t)
        db.session.add(student)
        db.session.commit()
        flash(_('details updated successfully'))
        return redirect(url_for('student.edit_student_profile'))
    form.tags.data = [str(tag.id) for tag in current_user.tags]
    return render_template('edit_profile.html', title = _('Edit Profile'), form = form)
