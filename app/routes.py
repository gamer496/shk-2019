from app import app
from flask import render_template
from flask_login import login_required, current_user
from flask import redirect, url_for

@app.route("/index")
def index():
    return render_template("index.html")

@app.route("/explore")
def explore():
    return render_template("explore.html")

@app.route("/profile")
@login_required
def profile():
    if hasattr(current_user, "company_link"):
        return redirect(url_for("company.company_edit_profile"))
    return redirect(url_for("student.edit_student_profile"))